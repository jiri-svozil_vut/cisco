## Zapojení:



## Part 1: Configure and verify IP addressing
- Configure IPv4 addresses according to the addressing table and the topology figure. Verify functional ping between neighbors (use a crossover cable between routers).
- Set static IP on PC1 and PC2 and verify connectivity between them. Later, for the DHCP setup step, discard the static IP.

Zapojení jsem provedl dle obrázku. <br>
**S1-L3** je spojený s **S2-L3** `fa0/10-11`<br>
**S1-L3** je spojený s **S3** `fa0/15-16`<br>
**S2-L3** je spojený s **S3** `fa0/5-6`<br>
**PC1** je s **S3** na `fa0/24`<br>
**PC2** je s **S2-L3** na `fa0/24`<br>

Dále jsem nastavil adresu na **PC1**: `192.168.20.2/24`, a na **PC2**: `192.168.20.3/24` a vyzkoušel `ping` mezi nimi.

## Part 2: Implement and Verify an EtherChannel between the switches

- Configure an Etherchannel with LACP between switches S1, S2 and S3.

Toto nastavení se týká prvků: **S1-L3**, **S2-L3**, **S3**. 
S tím, že je uveden LACAP, je konfigurace jednodušší... Je zde důležité si dobře zvolit názvy port-chanelů... Tak aby se to nebilo. Proto volím následující:

**S1-L3** - **S2-L3** ,

| Device 1 | Device 2 | Ports     | Port Channel |
|----------|----------|-----------|--------------|
| S1-L3    | S2-L3    | fa0/10-11 | 1            |
| S1-L3    | S3       | fa0/15-16 | 2            |
| S2-L3    | S3       | fa0/5-6   | 3            |

### S1-L3
#### Port Channel 1
```
conf t
interface range fa0/10-11
shutdown
channel-group 1 mode active
no shutdown
interface port-channel 1
switchport mode trunk
exit
```
#### Port Channel 2
```
conf t
interface range fa0/15-16
shutdown
channel-group 2 mode active
no shutdown
interface port-channel 2
switchport mode trunk
exit
```
### S2-L3
#### Port Channel 1
```
conf t
interface range fa0/10-11
shutdown
channel-group 1 mode active
no shutdown
interface port-channel 1
switchport mode trunk
exit
```
#### Port Channel 3
```
conf t
interface range fa0/5-6
shutdown
channel-group 3 mode active
no shutdown
interface port-channel 3
switchport mode trunk
exit
```
### S3
#### Port Channel 2
```
conf t
interface range fa0/15-16
shutdown
channel-group 2 mode active
no shutdown
interface port-channel 2
switchport mode trunk
exit
```
#### Port Channel 3
```
conf t
interface range fa0/5-6
shutdown
channel-group 3 mode active
no shutdown
interface port-channel 3
switchport mode trunk
exit
```


Správné nastavení se dá ověřit příkazem `show etherchannel summary` a Port Channel's by měli být vidět v `show ip interface brief`

## Part 3: Create VLANs and Assign Switch Ports

- Create VLAN 10 (Laboratory), VLAN 20 (Office) and VLAN 99 (Native). 
Zde se VLANy nastavují na všech switchích
```
conf t
vlan 10
name Laboratory
vlan 20
name Office
vlan 99
name Native
end
```

Příkazy je možné si ověřit `show vlan brief`

- Assign corresponding switch ports to the correct VLAN.  

Dále je nunté přiřadit fyzické porty k VLAN - ke správnému zařízení v tomto případě to bude f0/24 k PC-2 (VLAN 20). A f0/24 k PC-1 (VLAN10)

### L3-S2

```
config t
interface f0/24
switchport mode access
switchport access vlan 20
```

### S3

```
config t
interface f0/24
switchport mode access
switchport access vlan 10
```

## Part 4: Configure an 802.1Q Trunk
- Manually configure trunks between S1, S2 and S3 with native VLAN 99.
- Specify that only used VLANs are allowed to cross the trunk.

Pokud není EtherChannel tak se dává interface kterým jsou switche spojene
### S1-L3
```
conf t
interface Port-channel1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

```
conf t
interface Port-channel2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

### S2-L3
```
conf t
interface Port-channel1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

```
conf t
interface Port-channel3
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

### S3
```
conf t
interface Port-channel2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

```
conf t
interface Port-channel3
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 99
switchport trunk allowed vlan 10,20,99
```

Kontrolu provedem `show interfaces trunk` - musí tam být ON.


## Part 5: Configure Inter-VLAN Routing
- Configure Inter-VLAN routing on S1-L3 and S2-L3 switches. 

### S1-L3
```
conf t
interface vlan 20
ip address 192.168.20.253 255.255.255.0
interface vlan 10
ip address 192.168.10.253 255.255.255.0
exit
ip routing
```

### S2-L3
```
conf t
interface vlan 20
ip address 192.168.20.254 255.255.255.0
interface vlan 10
ip address 192.168.10.254 255.255.255.0
exit
ip routing
```
- The interface connected to the router will be the routed interface.

### S1-L3
```
conf t
interface g0/1
no switchport
ip address 192.168.1.2 255.255.255.0
```

### S2-L3
```
conf t
interface g0/1
no switchport
ip address 192.168.2.2 255.255.255.0
```

Ověřit jde: `show ip interface brief`


### Part 6: Configure and verify static and default routing
- Configure static routing so that clients reach the loopback on R2 (use the default routes in the direction to the loopback. In the opposite direction you need to use specific routes). Static routes must be set for all networks in the topology, i.e. set on all routers and L3 switches.


### R1
Napřed nastavíme IP Adressy pro interfacy
```
conf t
interface g0/0 
ip address 172.16.1.2 255.255.255.0 
no shutdown 
interface g0/1 
ip address 192.168.1.1 255.255.255.0 
no shutdown 
exit 
```

### R3
Napřed nastavíme IP Adressy pro interfacy
```
conf t
interface g0/0 
ip address 192.168.2.1 255.255.255.0 
no shutdown 
interface g0/1 
ip address 172.16.2.2 255.255.255.0 
no shutdown 
exit 
```

### R2
Napřed nastavíme IP Adressy pro interfacy
```
conf t
interface g0/0 
ip address 172.16.1.1 255.255.255.0 
no shutdown 
interface g0/1 
ip address 172.16.2.1 255.255.255.0 
no shutdown 
interface loopback 1
ip address 10.1.0.1 255.255.255.0
exit 
```

Nyní nastavíme Routy... 


### R1

```
conf t
ip route 0.0.0.0 0.0.0.0 172.16.1.1
ip route 192.168.20.0 255.255.255.0 192.168.1.2 
ip route 192.168.10.0 255.255.255.0 192.168.1.2 
end
```

### R3

```
conf t
ip route 0.0.0.0 0.0.0.0 172.16.2.1
ip route 192.168.20.0 255.255.255.0 192.168.2.2 
ip route 192.168.10.0 255.255.255.0 192.168.2.2 
end
```

### R2

```
conf t
ip route 192.168.1.0 255.255.255.0 172.16.1.2
ip route 192.168.10.0 255.255.255.0 172.16.1.2
ip route 192.168.20.0 255.255.255.0 172.16.1.2

ip route 192.168.2.0 255.255.255.0 172.16.2.2
ip route 192.168.10.0 255.255.255.0 172.16.2.2
ip route 192.168.20.0 255.255.255.0 172.16.2.2
end
```

Na switchich L3 musíme nastavit default gateway

### S1-L3

```
conf t
ip route 0.0.0.0 0.0.0.0 192.168.1.1
end
```

### S2-L3

```
conf t
ip route 0.0.0.0 0.0.0.0 192.168.2.1
end
```


## Part 7: Configure and verify DHCPv4
- Configure the DHCP server on R1. PC1 and PC2 get IP addresses from the DHCP server (PC1 - VLAN 10 gets an IP address from the range 192.168.10.1 - 192.168.10.10, PC2 - VLAN 20 gets an IP address from the range 192.168.20.1 - 192.168.20.10).

Budem dělat adresy v rozsahu .100 - .120
### R1

#### DHCP pro VLAN10
```
conf t
ip dhcp excluded-address 192.168.10.100 192.168.10.254 
ip dhcp pool V10-pool 
network 192.168.10.0 255.255.255.0 
domain-name ccna-lab.com
default-router 192.168.10.253
end
```

#### DHCP pro VLAN20
```
conf t
ip dhcp excluded-address 192.168.20.100 192.168.20.254  
ip dhcp pool V20-pool 
network 192.168.20.0 255.255.255.0 
domain-name ccna-lab.com
default-router 192.168.20.253
end
```

Musí se nastavit ještě DHCP Relay Agent na S1-L3

#### S1-L3

```
conf t
interface vlan10
ip helper-address 192.168.1.1
end
```
```
conf t
interface vlan20
ip helper-address 192.168.1.1
end
```


## Part 8: Configure an HSRP
- Configure HSRP on S1-L3 and S2-L3. Switch S1-L3 will be active router for VLANs 10 and 20. S2-L3 will be standby router. The IP address of the virtual default gateway for VLAN 10 will be: 192.168.10.100/24. The IP address of the virtual default gateway for VLAN 20 will be: 192.168.20.100/24.

Nebudu to dělat


## Part 9: Switch Security Configuration
- On S3, enable port security on access port with the following settings: the sticky method, maximum number of MAC addresses - 2. (Secure only the used link to the connected station). Disable unused ports. Configure DHCP snooping.

Sticky method

```
conf t
interface f0/24
switchport port-security
switchport port-security mac-address sticky
switchport port-security maximum 2

switchport port-security aging time 60
switchport port-security violation protect
```

info o by mělo jít zobrazit `show port-security interface f0/24` a `show port-security address`
Dále je nutné vypnout porty... 
do ParkingLot vlany to není nutné dávat..
```
interface range f0/2-4 , f0/7-24, g0/1-2
switchport mode access
switchport access vlan 999
shutdown
```


