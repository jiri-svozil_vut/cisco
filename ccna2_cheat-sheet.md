## VLAN

Vytvoření VLAN
```
conf t
interface vlan <num_of_vlan>
```

Přiřazení k portu
```
conf t
interface <num_of_interface>
switchport mode access
switchport access vlan <num_of_vlan>
```

Pokud se jedná o VoIP vlan:
```
conf t
interface <num_of_interface>
mls qos trust cos
switchport voice vlan <num_of_vlan>
```

## Trunk
```
conf t
switchport mode trunk
```
Nebo
```
conf t
switchport mode dynamic desirable
```
Vypnout DTP negotiation 
```
switchport nonegotiate
```
```
switchport trunk encapsulation dot1q
switchport trunk native vlan <num_of_vlan>
switchport trunk allowed vlan <num_of_vlan>,<num_of_vlan>,<num_of_vlan>,<num_of_vlan>....
```

## Router on Stick - Směrování mezi VLAN 
Na switch musím ještě udělat:
```
conf t
ip routing
```
Potom musím na to jít přes router, protože jeden ten port routeru musím rozdělit na podčásti podle toho kolik mam vlanek


Npař. vlan 3 -> 
```
conf t
interface <interface>.<num_of_vlan> (interface g0/0/1.3)
ip address <address> <mask>
description <popis vlanky>
encapsulation dot1q <num_of_vlan>
```
Pokud je vlan native potom `encapsulation dot1q <num_of_vlan> native` - Nativní vlan nemá ip adresu.<br>
Potom sa switchích nastavím na vlan intefacech ip adresy a nastavím default gateway a ip adresu routeru...

## DHCPv4
Vyčlenění adres mimo DHCP
```
conf t
ip dhcp excluded-address <start-range-address> <end-range-address>
```
Vytvoření poolu
```
conf t
ip dhcp pool <pool_name>
network <network_address-and-mask>
default-router <router-address>
domain-name ccna-lab.com
dns-server <server-url_or_address>
lease <days> <hours> <minutes>
interface <interface> # Pool musím přiřadit k portu na kterým bude dostupný
ip dhcp server <pool-name>
```

Pokud je potřeba udělat dhcp helpera, jdu na router kde ho chci udělat

```
conf t
interface <interface> # interface na kterým chci aby byl dostupný
ip helper-address <router-address> # ip adresa routeru na kterým běží dhcp
```

Pokud chci, aby si některý interface routeru dostal ip adresu pomocí dhcp
```
conf t
interface <interface> # interface na kterým se to má stát
ip address dhcp
no shutdown
```

## DHCPv6 stateful
```
conf t
ipv6 dhcp pool <pool_name>
address prefix <ipv6 adresa serveru>
dns-server <adresa dns serveru>
domain-name STATEFUL.com
int <interface> #Přiřadím k interface 
ipv6 dhcp server <pool_name>
```

## DHCPv6 stateless
```
conf t
ipv6 dhcp pool <pool_name>
address prefix <ipv6 adresa serveru>
dns-server <adresa dns serveru>
domain-name STATEFUL.com
int <interface> #Přiřadím k interface 
ipv6 nd other-config-flag
ipv6 dhcp server <pool_name>
```
Pak relay agent: 

Momentálně nevím #TODO: Zjistit, dodělat

## HSRP
```
conf t
int <interface>
standy version 2 #(dvojka podporuje ipv6) 
standby 1 <1= group number> ip <IP virtual gateway>
standby 1 priority <priority> # (defaultne 100) – vyšší číslo = nižší priorita
standby 1 preempt # (na oba) Založní nastavuju identicky, jiná priorita

```

## Port security
`switchport port-security` – zabezpečí port/porty <br>
`switchport port-security maximum 1` -> na port se připojí jen jedno zařízení<br>
`switchport port-security mac-address sticky` -> mac adresa se dynamicky získá a pak si ji zapamatuje<br>
`switchport port-security violation restrict` -> když dojde k napadení, nevypne se port ale ohlásí napadení a začne zahazovat pakety<br>


## IPv4 Static routing 

```
ip route <destination_address> <destination_mask> <next-hop-address>
```

## IPv6 static routing

```
ipv6 route <destination_address_and_mask> <next-hop-address> <link-local-adresa>
```

```
ipv6 route<destination_address_and_mask> <next-hop-address> 
```
