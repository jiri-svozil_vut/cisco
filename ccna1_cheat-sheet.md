## Základní nastavení Switche


Nastavení výchozího template pro použití IPV4 a Ipv6 zároveň <br>
    
    conf t
	sdm prefer dual-ipv4-and-ipv6 default
	end
	reload

Všeobecné nastavení na začátku<br>

    enable
    conf t
    no ip domain-lookup
    logging synchronous
Nastavení jména<br>

	configure terminal
	hostname
Nastavení hesla konzole<br>
	
    conf t
	line console 0
	password <pass>
	login
	exit

Heslo privilegovaný mod<br>

	conf t
	enable password <cisco>
	exit
Šifrovaný heslo privilegovaný mod<br>
	
    conf t
	enable secret <secret_pass>
	exit
Zašifrování všech hesel<br>

	conf t
	service password-encryption
Banner MOTD<br>
	
    conf t
	banner motd $ Authorizated Only! $
	exit
Nastavení IP adresy switche<br>

	conf t
	interface vlan 1
	ip address 192.168.1.2 255.255.255.0
	no shutdown
	exit
ARP tabulka<br>
* zobrazení tabulky `arp-a`
* vymazání tabulky `arp-d`

Nastavení IPv6<br>

	conf t
	ipv6 unicast-routing
	interface gigabitethernet 0/0
	ipv6 address 2001:db8:1:1::1/64
	ipv6 address fe80::1 link-local
	no shutdown 
Nastavení SSH nebo telnet<br>

	conf t
	ip domain-name CCNA.com
	username <user> secret <password>
	crypto key generate rsa <1024 or 2048>
	login block-for <180 (sekundy na jak dlouho se to blokuje)> attempts <4 (počet pokusů)> within <60 (během kolika sekund ty 4 pokazím)>
	line vty 0 4
	transport input <ssh (nebo telnet, podle toho co nastavuju)>
	login local
	exec-timeout <6 (minuty za jak dlouho po neaktivitě mě to odhlásí z ssh)>
Přihlášení se do ssh ne pomocí teraterm ale příkazovýho řádku switche nebo routeru<br>
`ssh -l admin <username> <address>`
* přerušení spojení `ctrl+shift+6`
* návrat - 2x enter
* ukončení spojení `exit`

Zkopírování nastavených dat do startup-config<br>

	conf t
	copy running-config startup-config
Hromadné vypnutí nepoužívaných portů<br>

	interface range F0/2-24
	shutdown

## Základní nastavení Routeru

Nastavení výchozího template pro použití IPV4 a Ipv6 zároveň<br>
    
    conf t
	sdm prefer dual-ipv4-and-ipv6 default
	end
    reload
všeobecný nastavení na začátku<br>

    enable
    conf t
    no ip domain-lookup
    logging synchronous
Nastavení jména<br>

	configure terminal
	hostname
Heslo konzole<br>

	line console 0
	password <password (cisco)>
	login
	exit


Heslo privilegovaný mod<br>
	 
    conf t
	enable password <password (cisco)>
	exit
 
Šifrovaný heslo privilegovaný mod<br>

	conf t
	enable secret <secret (class)>
	exit
Zašifrování všech hesel<br>

	conf t
	service password-encryption
    exit

Minimální délka hesla<br>

	security password min-length 10
Banner MOTD<br>

	conf t
	banner motd $ Authorized Only!!! $
	exit
Nastavení interface<br>
podívat se jak jsou přesně ty interface pojmenovaný

	conf t
	interface <interface (Ge0/0)>
	ip address <addresa> <maska>
	no shutdown
    description <text (LAN connection to S1)>


Vytvoření a nastavení loopback<br>

    conf t
	interface loopback <číslo loopbacku>
Nemusím použít no shutdown, mělo by se to zapnout samo
Použiju spíš vlastní subnet nebo funguje 1.1.1.1 255.255.255.255
	
Nastavení IPv6<br>

	conf t
	ipv6 unicast-routing
	interface <interface (Ge0/0)>
	ipv6 address <address (2001:db8:1:1::1/64)>
	ipv6 address <address (fe80::1)> link-local
	no shutdown
Nastavení SSH nebo telnet<br>

	conf t
	ip domain-name CCNA.com
	username <user> secret <password>
	Crypto key generate rsa <1024 or 2048>
	login block-for <180 (sekundy na jak dlouho se to blokuje)> attempts <4 (počet pokusů)> within <60 (během kolika sekund ty 4 pokazím)>
	line vty 0 4
	transport input <ssh (nebo telnet, podle toho co nastavuju)>
	login local
	exec-timeout <6 (minuty za jak dlouho po neaktivitě mě to odhlásí z ssh)>
ARP tabulka<br>
* zobrazení tabulky `arp-a`
* vymazání tabulky `arp-d`

Zkopírování nastavených dat do startup-config<br>

	conf t
	copy running-config startup-config
Hromadné vypnutí nepoužívaných portů<br>

	interface range F0/0-2
	shutdown





## Sítě
Dělení na třídy:
- A (0 – 127)
- B (128 – 191) 
- C (192 – 223)
- D
- E


Mění se se zařízením - 2001:db8:acad:1::**1** <br>
Mění se s podsítí - 2001:db8:acad:**1**::1 <br>
Výpočet počtu hostů z masky: 
/25 => 32-25 = 7 => 2^7=128 hostů (počítá se do toho i adresa sítě a broadcast)

## Tabulka masky sítí

|     | Net Mask        | Number of hosts | Amount of Class C |
|-----|-----------------|-----------------|-------------------|
| /16 | 255.255.0.0     | 65534           | 256               |
| /17 | 255.255.128.0   | 32766           | 128               |
| /18 | 255.255.192.0   | 16382           | 64                |
| /19 | 255.255.224.0   | 8190            | 32                |
| /20 | 255.255.240.0   | 4094            | 16                |
| /21 | 255.255.248.0   | 2046            | 8                 |
| /22 | 255.255.252.0   | 1022            | 4                 |
| /23 | 255.255.254.0   | 510             | 2                 |
| /24 | 255.255.255.0   | 254             | 1                 |
| /25 | 255.255.255.128 | 126             | 1/2               |
| /26 | 255.255.255.192 | 62              | 1/4               |
| /27 | 255.255.255.224 | 30              | 1/8               |
| /28 | 255.255.255.240 | 14              | 1/16              |
| /29 | 255.255.255.248 | 6               | 1/32              |
| /30 | 255.255.255.252 | 2               | 1/64              |

